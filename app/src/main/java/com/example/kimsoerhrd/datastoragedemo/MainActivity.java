package com.example.kimsoerhrd.datastoragedemo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {


    EditText edUser, edPass;
    Button btnSave, btnShow, btnW, btnR, btnWrite, btnRead;

    String data="Hello World";
    String myfile ="myFile.txt";
    String tmp="";

    File myExternal;
    String filePath="MyFileStorage";
    String fileName = "Sample.txt";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edPass = findViewById(R.id.edPass);
        edUser = findViewById(R.id.edUser);
        btnSave = findViewById(R.id.btnSave);
        btnShow = findViewById(R.id.btnShow);
        btnW = findViewById(R.id.btnWrite);
        btnR = findViewById(R.id.btnRead);
        btnRead = findViewById(R.id.btnR);
        btnWrite = findViewById(R.id.btnW);

        if (!isExternalStorageAvailable() || isExternalStorageReadOnly()){
            btnWrite.setEnabled(true);
        }else {

          myExternal =getPublicAlbumStorageDir(fileName);
        }

        btnWrite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {

                    if (!hasRuntimePermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        requestRuntimePermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE, 1);
                    }
                    FileOutputStream fos = new FileOutputStream(myExternal);
                    fos.write(edUser.getText().toString().getBytes());
                    fos.write(edPass.getText().toString().getBytes());
                    fos.close();
                    edUser.setText("");
                    edPass.setText("");
                    Toast.makeText(getApplicationContext(),"Success Save", Toast.LENGTH_LONG).show();

                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        btnRead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileInputStream fis = new FileInputStream(myExternal);
                    DataInputStream inputStream = new DataInputStream(fis);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                    String strLine;
                    while ((strLine = reader.readLine())!=null){
                        data = data+strLine;
                    }
                    edUser.setText(data);
                    inputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });




        btnW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileOutputStream outputStream = openFileOutput(myfile, Context.MODE_PRIVATE);
                    outputStream.write(edUser.getText().toString().getBytes());
                    outputStream.write(edPass.getText().toString().getBytes());
                    edUser.setText("");
                    edPass.setText("");
                    outputStream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        btnR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    FileInputStream inputStream = openFileInput(myfile);
                    InputStreamReader reader = new InputStreamReader(inputStream);
                    BufferedReader bufferedReader = new BufferedReader(reader);
                    String value;
                    while ((value=bufferedReader.readLine())!=null){
                        tmp+=value+"\n";
                    }
                    edUser.setText(tmp);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });



        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences share= MainActivity.this.getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = share.edit();
                editor.putString("username",edUser.getText().toString());
                editor.putString("pass", edPass.getText().toString());
                editor.commit();
                edPass.setText(" ");
                edUser.setText(" ");
            }
        });

        btnShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = MainActivity.this.getPreferences(Context.MODE_PRIVATE);
                edUser.setText(sharedPreferences.getString("username",null));
                edPass.setText(sharedPreferences.getString("pass",null));
            }
        });

    }

    private  static boolean isExternalStorageReadOnly(){
        String extStorage = Environment.getExternalStorageState();
        Log.e("Storage", "datastorage"+extStorage);
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(extStorage)){
            return true;
        }
        return false;
    }

    private static boolean isExternalStorageAvailable(){
        String extStorage = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(extStorage)){
            return true;
        }
        return false;
    }

    public File getPublicAlbumStorageDir(String albumName){
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), albumName);
        if (!file.mkdir()){
            Log.e("data", "Directory not created");
        }
        return file;
    }

    boolean hasRuntimePermission(Context context, String runtimePermission){
        boolean value=false;
        int currentAndroidVersion = Build.VERSION.SDK_INT;
        if (currentAndroidVersion > Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(context, runtimePermission)== PackageManager.PERMISSION_GRANTED){
                value = true;
            }
        }else {
            value = true;
        }
        return value;
    }

    void requestRuntimePermission(Activity activity, String runtimePermission, int requestCode){
        ActivityCompat.requestPermissions(activity, new String[]{runtimePermission}, requestCode);
    }

}
